import React from 'react';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { Container, Row, Col } from 'react-bootstrap';
import profileImage from '../images/profileImage.jpg';

export default function Home() {
  return (
  	<React.Fragment>
	  	<Head>
	  		<title>Hollnsteiner Portfolio - About</title>
	  	</Head>
		<Container id="aboutContainer">
			<Row>
				<Col xs={12} md={6}>
					<img
						src={profileImage}
						alt="Picture of Philip"
						width={500}
						height={600}
						id='profilePic'
					></img>
				</Col>
				<Col xs={12} md={6} id="descriptionContainer">
					<h1>Hi, Im Phil</h1>
					<p>I am an accomplished civil engineer looking to make a new career in the web development field. 
					I have completed an intensive four month coding bootcamp curriculum with honors and developed skills in both front end and back end web development. 
					I am interested in joining a diverse team of web developers where I can effectively use my skills as well as grow my technical knowledge.
					</p>
					<span/>
					<p>Previously, I graduated from Rensselaer Polytechnic Institute in the class of 2013 with an Engineering Degree.
					I have 7 years of civil engineering experience in which I learned troubleshooting skills that will aid me in web development.
					</p>
					<span/>
					<p>In my free time I enjoy golfing and scuba diving.</p>
					<span/>
					<p>Please feel free to take a look at my previous projects in the projects tab and to visit my Linkedin profile here: <a target="_blank" href='https://www.linkedin.com/in/philip-hollnsteiner-e-i-t-a14014127/'>My LinkedIn Profile</a>
					</p>
					<span/>
					<h4>Technical Skills:</h4>
					<ul>
						<li>Git</li>
						<li>HTML</li>
						<li>CSS</li>
						<li>Bootstrap</li>
						<li>JavaScript</li>
						<li>MongoDB</li>
						<li>Express.JS</li>
						<li>React.JS</li>
						<li>Next.JS</li>
					</ul>
				</Col>
			</Row>
		</Container>
  	</React.Fragment>
  )
}