import React from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Head from 'next/head';
import styles from '../../styles/Home.module.css';
import emailjs from 'emailjs-com';
import Swal from 'sweetalert2';

export default function contact(){

	function sendEmail(e){
		e.preventDefault();

	    emailjs.sendForm('service_zru3csl', 'template_cqxgpqk', e.target, 'user_5HNAtOM2QrH7bMc3VPET0')
	      .then((result) => {
	          Swal.fire(
				  'Success',
				  'Message Successfully Sent',
				  'success'
			  );
	      }, (error) => {
	          Swal.fire(
					"Something went wrong",
					"Please try sending message again or email philiphollnsteiner@gmail.com directly",
					"error"
				)
	      });
	      e.target.reset()
	}

	return(
		<React.Fragment>
			<Head>
				<title>Hollnsteiner Portfolio - Contact</title>
			</Head>
			<Container id="messageForm">
				<Form onSubmit={sendEmail}>
					<Form.Group contorlId="firstName">
						<Form.Label>First Name:</Form.Label>
						<Form.Control type="text" placeholder="Type your first name here" name='firstName' required/>
					</Form.Group>
					<Form.Group contorlId="lastName">
						<Form.Label>Last Name:</Form.Label>
						<Form.Control type="text" placeholder="Type your last name here" name='lastName' required/>
					</Form.Group>
					<Form.Group contorlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Type your email here" name='email' required/>
					</Form.Group>
					<Form.Group contorlId="message">
						<Form.Label>Message:</Form.Label>
						<Form.Control as="textarea" rows="5" placeholder="Type your message here" name='message' required/>
					</Form.Group>

					<Button id="submitBttn" type="submit" value="Send Message">Submit</Button>
				</Form>
			</Container>
		</React.Fragment>
	)
}