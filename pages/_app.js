import React from 'react';
import NavBar from '../components/NavBar';
import Head from 'next/head';

import 'bootswatch/dist/litera/bootstrap.min.css';
import '../styles/globals.css'
import favicon from '../public/favicon-32x32.png';

function MyApp({ Component, pageProps }) {
  return(
  	<React.Fragment>
  		<Head>
	  		<link rel="shortcut icon" href={favicon}/>
	  		<title>Hollnsteiner Portfolio</title>
	  	</Head>
  		<NavBar/> 
  		<Component {...pageProps} />
  	</React.Fragment>
  )
}

export default MyApp
