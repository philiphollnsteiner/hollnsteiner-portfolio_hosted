import React from 'react'
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import logo from '../public/android-chrome-192x192.png';

export default function NavBar() {
	return(
		<Navbar id="navigationBar" expand="lg">
			<Link href='/'>
				<img
					src={logo}
					alt="logo"
					width={60}
					height={60}
					id='logoImg'
				></img>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" /> 
			<Navbar.Collapse id="basic-navbar-nav"> 
				<Nav className="mr-auto"> 
					<Link href="/">
						<a className="nav-link" role="button">About</a> 
					</Link> 
					<Link href="/projects"> 
						<a className="nav-link" role="button">Projects</a> 
					</Link> 
					<Link href="/contact"> 
						<a className="nav-link" role="button">Contact</a> 
					</Link>
				</Nav> 
			</Navbar.Collapse> 
		</Navbar> 
	)
}
